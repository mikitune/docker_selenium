# DockerでSelenium！
> このリポジトリはこちらの[ブログ記事](https://koneta.click/p/519.html)で作成したプログラムです。  

Selenium環境構築用のイメージです。Pythonとブラウザ(Chrome, Firefox)の環境構築も同時に行います。  
またヘッドレスで動作するため、GUIの無いサーバでも動作します。  

## 使い方
#### ノード数の調整方法
`docker-compose.tml`の`NODE_MAX_INSTANCES`, `NODE_MAX_SESSION` を変更することで、一つのコンテナ内で起動するノード数を設定することができます。
```yml
environment:
- NODE_MAX_INSTANCES=5
- NODE_MAX_SESSION=5
```
#### Python ライブラリのインストール
`python-selenium/requirements.txt`にインストールしたいライブラリを追記することで、ビルド時に自動でインストールされます。  
例えば`tqdm`(forループ時にプログレスバーを出してくれるライブラリ) を入れたい場合は、以下のようになります (seleniumは消さないでください)。
```
selenium
tqdm
```

#### プログラムの実行方法
```bash
$ # 開始
$ cd [docker-compose.ymlのあるディレクトリ]
$ docker-compose up -d
$ docker exec -it python python /root/script/sample.py
$ # 終了
$ docker-compose down
```

### VNC接続によるデバッグ
ブラウザはdebug用のイメージを使用しているため、`VNC`でアクセスすることで、ブラウザの動きをGUIで確認することができます。  
各ブラウザのコンテナのIPにポート(Chrome:5900番ポート, Firefox:5901番ポート)を指定してVNCアクセスで確認できます。  
接続時のパスワードは"secret"です。
