#!/usr/local/bin/python3
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def execSearch(browser: webdriver, img_name: str):
    """
    Googleで検索する
    :param browser: webdriver
    :param img_name: str
    """
    # Googleにアクセス
    browser.get('https://www.google.co.jp/')
    WebDriverWait(browser, 15).until(EC.presence_of_all_elements_located)

    # キーワードの入力
    search_box = browser.find_element_by_name("q")
    search_box.send_keys('hello selenium')

    # 検索実行
    search_box.submit()
    WebDriverWait(browser, 15).until(EC.presence_of_all_elements_located)

    # スクリーンショット
    browser.save_screenshot(img_name + '.png')

if __name__ == '__main__':
    try:
        # UA を設定
        capabilities = webdriver.common.desired_capabilities.DesiredCapabilities.CHROME.copy()
        capabilities['javascriptEnabled'] = True

        options = webdriver.ChromeOptions()
        options.add_argument('--user-agent="Mozilla/5.0 (Linux; Android 4.0.3; SC-02C Build/IML74K) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.58 Mobile Safari/537.31"')

        # HEADLESSブラウザに接続
        browser = webdriver.Remote(
            command_executor='http://selenium-hub:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.CHROME,
            options=options
        )

        browser2 = webdriver.Remote(
            command_executor='http://selenium-hub:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.FIREFOX
        )

        # FBで実行
        execSearch(browser,  img_name='chrome')
        execSearch(browser2, img_name='firefox')

        browser.close()
        browser2.close()

    finally:
        # 終了
        pass